package;
import flash.Lib;
import ru.deirel.fanhero.Keys;
import ru.deirel.fanhero.Player;
import ru.deirel.fanhero.Res;
import ru.deirel.fanhero.MenuScreen;
import ru.deirel.fanhero.Saved;
import ru.deirel.fanhero.ScreenManager;
import ru.deirel.fanhero.Updater;
import ru.deirel.fanhero.utils.M;
import starling.display.Sprite;
import starling.events.EnterFrameEvent;
import starling.events.Event;
import treefortress.sound.SoundManager;

/**
 * ...
 * @author 
 */
class App extends Sprite {
    static public var it:App;
    
    public var screenWidth:Float;
    public var screenHeight:Float;
    public var screenManager:ScreenManager;
    public var res:Res;
    public var updater:Updater;
    public var keys:Keys;
    public var sound:Player;
    public var saved:Saved;
    
    public function new() {
        super();
        it = this;
        
        screenWidth = Lib.current.stage.stageWidth;
        screenHeight = Lib.current.stage.stageHeight;
        
        saved = new Saved();
        updater = new Updater();
        keys = new Keys();
        sound = new Player();
        
        screenManager = new ScreenManager(this);
        res = new Res();
        
        Main.starling.stage.addEventListener(EnterFrameEvent.ENTER_FRAME, enterFrameHandler);
        
        screenManager.showScreen(new MenuScreen());
        
        sound.addPlaylist("bg", res.sounds);
    }
    
    private function enterFrameHandler(e:EnterFrameEvent):Void {
        updater.update(1/60);
    }
    
    override public function dispose():Void {
        Main.starling.stage.removeEventListener(EnterFrameEvent.ENTER_FRAME, enterFrameHandler);
        M.dispose(screenManager);
        M.dispose(updater);
        M.dispose(res);
        super.dispose();
    }
}