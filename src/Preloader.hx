package;
import flash.Lib;
import flash.display.Shape;
import flash.display.Sprite;
import flash.events.Event;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;

/**
 * ...
 * @author 
 */
class Preloader extends Sprite {

    static public function main() {
        Lib.current.addChild(new Preloader());
    }
    
    private var bar:Shape;
    private var label:TextField;
    
    public function new() {
        super();
        createBar();
        createLabel();
        addEventListener(Event.ENTER_FRAME, enterFrameHandler);
    }
    
    private function createBar() {
        bar = new Shape();
        bar.graphics.beginFill(0xeebb03);
        bar.graphics.drawRect(0, 0, 32, 32);
        bar.width = 0;
        addChild(bar);
    }
    
    private function createLabel() {
        label = new TextField();
        label.defaultTextFormat = new TextFormat("Verdana", 22, 0x000000, true, false, false, null, null, TextFormatAlign.CENTER);
        label.width = Lib.current.stage.stageWidth;
        label.height = bar.height;
        addChild(label);
    }
    
    private function enterFrameHandler(_) {
        var bytesLoaded = Lib.current.stage.loaderInfo.bytesLoaded;
        var bytesTotal = Lib.current.stage.loaderInfo.bytesTotal;
        var percent = bytesLoaded / bytesTotal;
        bar.width = Lib.current.stage.stageWidth * percent;
        label.text = 'Loading... ${Math.round(percent * 100)} %';
        if (percent == 1) {
            removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
            Lib.current.removeChild(this);
            var cls = Type.resolveClass("Main");
            var app = Type.createInstance(cls, []);
            Lib.current.addChild(app);
        }
    }
}