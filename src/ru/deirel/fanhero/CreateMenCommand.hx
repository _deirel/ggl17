package ru.deirel.fanhero;

import mmvc.impl.Command;
import msignal.Signal.Signal1;
import ru.deirel.fanhero.GameModel;

/**
 * ...
 * @author 
 */
class CreateMenCommand extends Command {
    @inject public var info:CreateMenInfo;
    @inject public var gameModel:GameModel;
    
    override public function execute():Void {
        gameModel.clear();
        gameModel.setXLimit(info.count + 4);
        gameModel.addIds([for (i in 0...info.count) i], info.playerNum);
    }
}

class CreateMenSignal extends Signal1<CreateMenInfo> { public function new() super(CreateMenInfo); }

class CreateMenInfo {
    public var count:Int;
    public var playerNum:Int;
    public function new(count:Int, playerNum:Int) {
        this.count = count;
        this.playerNum = playerNum;
    }
}