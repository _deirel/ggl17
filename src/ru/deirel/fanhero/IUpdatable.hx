package ru.deirel.fanhero;

/**
 * @author 
 */
interface IUpdatable {

    function update(dt:Float):Void;
}