package ru.deirel.fanhero;

import mmvc.api.IViewContainer;
import ru.deirel.fanhero.ViewBase;
import starling.display.DisplayObject;
import starling.display.Sprite;

/**
 * ...
 * @author 
 */
class ViewContainer extends Sprite implements IViewContainer {
    public var viewAdded:Dynamic->Void;
    public var viewRemoved:Dynamic->Void;
    
    public function new() {
        super();
    }
    
    public function isAdded(view:Dynamic):Bool {
        try {
            var typed = cast(view, ViewBase);
            return typed.isAddedToViewContainer();
        } catch (err:String) {
            return false;
        }
    }
    
    public function add(view:DisplayObject):Void {
        if (viewAdded != null) {
            viewAdded(view);
        }
    }
    
    public function remove(view:DisplayObject):Void {
        if (viewRemoved != null) {
            viewRemoved(view);
        }
    }
}