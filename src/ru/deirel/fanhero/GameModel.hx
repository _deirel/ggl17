package ru.deirel.fanhero;
import flash.ui.Keyboard;
import motion.Actuate;
import msignal.Signal.Signal0;
import msignal.Signal.Signal1;
import ru.deirel.fanhero.CreateMenCommand.CreateMenInfo;
import ru.deirel.fanhero.CreateMenCommand.CreateMenSignal;
import ru.deirel.fanhero.Rater.Rate;
import ru.deirel.fanhero.utils.M;
import ru.deirel.fanhero.utils.MathUtil;
import ru.deirel.fanhero.utils.UniqueId;

/**
 * ...
 * @author 
 */
class GameModel implements IUpdatable {
    static private var _uid:UniqueId = new UniqueId();
    
    @inject public var wavesGenerator:WavesGenerator;
    @inject public var rateSignal:WaveRateSignal;
    @inject public var createMenSignal:CreateMenSignal;
    @inject public var config:GameConfig;
    
    public var onIdsAdded:Signal1<Array<WaveId>> = new Signal1();
    public var onClear:Signal0 = new Signal0();
    public var onHit:Signal1<WaveHitInfo> = new Signal1();
    
    private var waves:Array<Wave> = new Array();
    private var ids:Array<WaveId> = new Array();
    private var xLimit:Float = 0.0;
    private var playerId:WaveId;
    private var lastWave:Wave = null;
    
    private var rater:Rater;
    
    public function new() {}
    
    @post public function postConstruct() {
        rater = new Rater(config.rateSpecificStep, config.maxRate, config.advancedWorstTime);
    }
    
    public function start() {
        App.it.keys.onKeyPress.add(keyPressHandler);
        createMenSignal.dispatch(new CreateMenInfo(10, 6));
    }
    
    public function startGameProcess() {
        App.it.sound.playlist("bg");
        wavesGenerator.start();    
        App.it.updater.add(wavesGenerator);
    }
    
    private function addId(x:Float, isPlayer:Bool) {
        var uid = _uid.get();
        var id = { x:x, id:uid, isPlayer:isPlayer };
        ids.push(id);
        ids.sort(function(i1, i2) {
            var dx = i2.x - i1.x;
            return Math.abs(dx) < 0.001 ? 0 : dx < 0 ? -1 : 1;
        });
        if (isPlayer) {
            playerId = id;
        }
        return id;
    }
    
    public function addIds(coords:Array<Float>, playerNum:Int) {
        var ids = [for (i in 0...coords.length) addId(coords[i], i == playerNum)];
        onIdsAdded.dispatch(ids);
    }
    
    public function setXLimit(value:Float) xLimit = value;
    
    public function clear():Void {
        wavesGenerator.stop();
        waves = [];
        ids = [];
        lastWave = null;
        playerId = null;
        onClear.dispatch();
    }
    
    public function createWave(velocity:Float, impulse:Float):Void {
        var w:Wave;
        waves.unshift(w = new Wave(velocity, impulse));
        w.worstDistance = rater.getWorstDistance(w.velocity);
    }
    
    private function keyPressHandler(keyCode:UInt):Void {
        if (keyCode == Keyboard.SPACE) {
            hitPlayer();
        }
    }
    
    public function getCurrentLevel():Int {
        return wavesGenerator.getLevel();
    }
    
    public function update(dt:Float):Void {
        for (w in waves) {
            w.update(dt);
        }
        var i = waves.length;
        while (i-- > 0) {
            var w = waves[i];
            if (w.x >= xLimit) {
                waves.splice(i, 1);
                lastWave = w;
            } else {
                for (id in ids) {
                    if (MathUtil.gte(w.x, id.x)) {
                        if (w.currentId == id.id) break;
                        w.currentId = id.id;
                        if (id.id != playerId.id) {
                            hit(id, w.impulse);
                        }
                        break;
                    }
                }
                if (!w.wasMissed && !w.wasHited && w.x - playerId.x >= w.worstDistance) {
                    w.wasMissed = true;
                    rateSignal.dispatch(new WaveRateInfo(playerId, rater.getWorstRate()));
                }
            }
        }
    }
    
    private function hit(id:WaveId, impulse:Float, force:Bool = false):Void {
        onHit.dispatch({ id:id, impulse:impulse });
    }
    
    public function hitPlayer():Void {
        if (playerId != null) {
            var minDist:Null<Float> = null;
            var nearestWave:Wave = null;
            if (waves.length > 0) {
                for (w in waves) {
                    var dist = Math.abs(w.x - playerId.x);
                    if (!w.wasMissed && (minDist == null || dist < minDist)) {
                        minDist = dist;
                        nearestWave = w;
                    }
                }
            } else if (lastWave != null && !lastWave.wasMissed) {
                minDist = Math.abs(lastWave.x - playerId.x);
                nearestWave = lastWave;
            }
            if (minDist != null) {
                hit(playerId, nearestWave.impulse);
                var rate:Rate = null;
                if (nearestWave.hasSecondChance || !nearestWave.wasHited) {
                    var time = minDist / nearestWave.velocity;
                    rate = rater.getRate(time, nearestWave.velocity);
                    if (!nearestWave.hasSecondChance && rate.value <= rater.getTooBadToRemember().value) {
                        nearestWave.hasSecondChance = true;
                    } else {
                        nearestWave.hasSecondChance = false;
                    }
                    nearestWave.wasHited = true;
                } else {
                    rate = rater.getWorstRate();
                }
                rateSignal.dispatch(new WaveRateInfo(playerId, rate));
            } else {
                hit(playerId, config.jumpDefaultPeriod);
            }
        }
    }
    
    public function setPause(value:Bool):Void {
        App.it.updater.setPause(value);
        if (value) {
            App.it.keys.onKeyPress.remove(keyPressHandler);
            Actuate.pauseAll();
            App.it.sound.pauseAll();
        } else {
            App.it.keys.onKeyPress.add(keyPressHandler);
            Actuate.resumeAll();
            App.it.sound.resumeAll();
        }
    }
    
    public function dispose():Void {
        App.it.updater.remove(wavesGenerator);
        App.it.sound.stopAll();
        App.it.keys.onKeyPress.remove(keyPressHandler);
        onIdsAdded.removeAll();
        onClear.removeAll();
        onHit.removeAll();
        waves = null;
        lastWave = null;
    }
}

typedef WaveId = {
    var id:Int;
    var x:Float;
    var isPlayer:Bool;
}

class Wave {
    public var x:Float = 0.0;
    public var velocity:Float;
    public var impulse:Float;
    public var currentId:Int = -1;
    public var wasHited:Bool = false;           // Скакнул ли чувак на волне
    public var wasMissed:Bool = false;          // Была ли эта волна пропущена игроком
    public var hasSecondChance:Bool = false;    // Дан ли шанс учесть оценку второй раз, если первая была плохой
    public var worstDistance:Float = 0.0;
    
    public function new(velocity:Float, impulse:Float) {
        this.velocity = velocity;
        this.impulse = impulse;
    }
    
    public function update(dt:Float):Void {
        x += velocity * dt;
    }
}

typedef WaveHitInfo = { id:WaveId, impulse:Float }

class WaveRateInfo { 
    public var id:WaveId;
    public var rate:Rate;
    public function new(id:WaveId, rate:Rate) {
        this.id = id;
        this.rate = rate;
    }
}

class WaveRateSignal extends Signal1<WaveRateInfo> { public function new() super(WaveRateInfo); }