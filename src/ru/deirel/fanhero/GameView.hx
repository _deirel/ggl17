package ru.deirel.fanhero;
import ru.deirel.fanhero.utils.RGBInterpolator;
import ru.deirel.fanhero.ViewBase;
import ru.deirel.fanhero.utils.M;
import ru.deirel.fanhero.utils.RGBInterpolator.RGB;
import starling.display.Quad;
import starling.events.TouchEvent;
import starling.events.TouchPhase;


/**
 * ...
 * @author 
 */
class GameView extends ViewBase implements IUpdatable {
    static public var EVENT_PAUSE_CLICKED:String = "pause_button_clicked";
    static public var EVENT_MUSIC_CLICKED:String = "music_button_clicked";
    static public var EVENT_PLAYLIST_PREV:String = "playlist_prev";
    static public var EVENT_PLAYLIST_NEXT:String = "playlist_next";
    static public var EVENT_BACKGROUND_CLICKED:String = "bg_clicked";
    
    private var menMap:Map<Int, Man> = new Map();
    private var menArray:Array<Man> = new Array();
    private var rateScale:RateScale;
    private var score:GameScore;
    private var activeShower:Shower;
    private var pauseButton:PauseButton;
    private var musicButton:MusicButton;
    private var playlistPanel:PlaylistPanel;
    private var bg:Quad;
    
    public function new() {
        super();
        bg = new Quad(App.it.screenWidth, App.it.screenHeight, 0xffffff);
        bg.addEventListener(TouchEvent.TOUCH, bgTouchHandler);
        addChild(bg);
    }
    
    public function init(rateColors:Array<RGB>):Void {
        rateScale = new RateScale(rateColors);
        rateScale.x = App.it.screenWidth / 2;
        rateScale.y = App.it.screenHeight / 2 - 110;
        addChild(rateScale);
        
        score = new GameScore();
        score.x = rateScale.x;
        score.y = rateScale.y + rateScale.height + 4;
        addChild(score);
        
        pauseButton = new PauseButton();
        pauseButton.alignPivot("right", "top");
        pauseButton.scale = 0.9;
        pauseButton.x = App.it.screenWidth - 16;
        pauseButton.y = 16;
        pauseButton.setHandler(pauseButtonHandler);
        addChild(pauseButton);
        
        musicButton = new MusicButton();
        musicButton.alignPivot("left", "top");
        musicButton.scale = 0.9;
        musicButton.x = 16;
        musicButton.y = 16;
        musicButton.setHandler(musicButtonHandler);
        addChild(musicButton);
        
        playlistPanel = new PlaylistPanel();
        playlistPanel.x = musicButton.x + musicButton.width + 16;
        playlistPanel.y = 16;
        playlistPanel.onPrev.add(playlistPanelPrevHandler);
        playlistPanel.onNext.add(playlistPanelNextHandler);
        addChild(playlistPanel);
    }
    
    public function addMan(id:Int, isPlayer:Bool, x:Float, y:Float, sx:Float = 1.0, sy:Float = 1.0, rot:Float = 0.0, directShow:Bool = true):Void {
        var man = new Man(isPlayer);
        menMap[id] = man;
        menArray.push(man);
        man.graphics.x = x;
        man.graphics.y = y;
        man.graphics.scaleX = sx;
        man.graphics.scaleY = sy;
        man.graphics.rotation = rot;
        if (!directShow) {
            man.graphics.visible = false;
        }
        addChild(man.graphics);
    }
    
    public function clearMen():Void {
        M.dispose(activeShower);
        for (m in menArray) m.dispose();
        menMap = new Map();
        menArray = [];
    }
    
    public function showMen(shower:Shower = null, callback:Void->Void = null):Void {
        if (shower == null) {
            for (m in menArray) m.graphics.visible = true;
            if (callback != null) {
                callback();
            }
        } else {
            activeShower = shower;
            shower.setObjects(cast menArray.map(function (m) return m.graphics));
            shower.animate(function() {
                activeShower.dispose();
                activeShower = null;
                if (callback != null) {
                    callback();
                }
            });
        }
    }
    
    public function jump(id:Int, height:Float, impulse:Float):Void {
        var man = menMap[id];
        if (man != null) {
            man.jump(height, impulse);
        }
    }
    
    public function showRatePopup(id:Int, text:String, color:UInt):Void {
        var man = menMap[id];
        if (man != null) {
            var popup = new RatePopup(text, color);
            popup.x = man.graphics.x;
            popup.y = man.graphics.y - 160;
            addChild(popup);
        }
    }
    
    public function showChangeLevelPopup(up:Bool):Void {
        var popup = new ChangeLevelPopup(up);
        popup.x = rateScale.x;
        popup.y = rateScale.y - 50;
        addChild(popup);
    }
    
    private function pauseButtonHandler(paused:Bool):Void {
        dispatchEventWith(EVENT_PAUSE_CLICKED, false, { paused: paused });
    }
    
    private function musicButtonHandler(enabled:Bool):Void {
        dispatchEventWith(EVENT_MUSIC_CLICKED, false, { enabled: enabled });
    }
    
    private function bgTouchHandler(event:TouchEvent):Void {
        var touch = event.getTouch(bg, TouchPhase.BEGAN);
        if (touch != null) {
            dispatchEventWith(EVENT_BACKGROUND_CLICKED, false);
        }
    }
    
    private function playlistPanelPrevHandler():Void {
        dispatchEventWith(EVENT_PLAYLIST_PREV, false);
    }
    
    private function playlistPanelNextHandler():Void {
        dispatchEventWith(EVENT_PLAYLIST_NEXT, false);
    }
    
    public function setFaces(id:Int):Void {
        for (m in menArray) m.setFace(id);
    }
    
    public function setRateScale(value:Float, isReset:Bool):Void {
        rateScale.setFill(value, isReset);
    }
    
    public function setRateLabel(value:String):Void {
        rateScale.setLabel(value);
    }
    
    public function setScore(value:Float):Void {
        score.setScore(value);
    }
    
    public function update(dt:Float):Void {
        for (m in menArray) m.update(dt);
    }
    
    public function togglePauseButton():Bool {
        return pauseButton.toggle();
    }
    
    public function toggleMusicButton():Void {
        musicButton.toggle();
    }
    
    public function changeMusicTitle(name:String):Void {
        playlistPanel.changeTitle(name);
    }
    
    override public function dispose():Void {
        clearMen();
        rateScale = null;
        super.dispose();
    }
}