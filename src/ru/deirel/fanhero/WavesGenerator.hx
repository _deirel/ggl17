package ru.deirel.fanhero;
import ru.deirel.fanhero.ChangeLevelCommand.LevelChangedInfo;
import ru.deirel.fanhero.ChangeLevelCommand.LevelChangedSignal;
import ru.deirel.fanhero.CreateWaveCommand.CreateWaveInfo;
import ru.deirel.fanhero.CreateWaveCommand.CreateWaveSignal;

/**
 * ...
 * @author 
 */
class WavesGenerator implements IUpdatable {
    @inject public var waveSignal:CreateWaveSignal;
    @inject public var levelChangedSignal:LevelChangedSignal;
    @inject public var config:GameConfig;
    
    private var currentDifficulty:DifficultyLevel;
    private var level:Int = -1;
    private var currentDelayer:Delayer;
    private var isActive:Bool = false;
    
    @post public function postConstruct() {
        setLevel(0);
    }
    
    public function start():Void {
        if (isActive) {
            return;
        }
        isActive = true;
        currentDelayer = delay(createWave, config.firstWaveTimeout);
    }
    
    private function createWave():Void {
        var waveInfo = generateWaveInfo();
        var timeout = getRandom(currentDifficulty.wavesTimeout) + waveInfo.impulse;
        waveSignal.dispatch(waveInfo);
        currentDelayer = delay(createWave, timeout);
    }
    
    public function stop():Void {
        if (currentDelayer != null) {
            currentDelayer.stop();
            currentDelayer = null;
        }
        isActive = false;
    }
    
    private function delay(foo:Void->Void, time:Float):Delayer {
        var delayer = new Delayer(time);
        delayer.run = foo;
        return delayer;
    }
    
    private function generateWaveInfo():CreateWaveInfo {
        var vel = 1 / (getRandom(currentDifficulty.waveRate));
        return new CreateWaveInfo(vel, getRandom(currentDifficulty.jumpPeriod));
    }
    
    private function getRandom(rng:RangeValue):Float {
        return rng.min + Math.random() * (rng.max - rng.min);
    }
    
    public function setLevel(value:Int):Void {
        if (value < 0) value = 0;
        if (level == value) {
            return;
        }
        var up = value > level;
        level = value;
        currentDifficulty = getCurrentDifficulty();
        levelChangedSignal.dispatch(new LevelChangedInfo(level, up));
    }
    
    private function getCurrentDifficulty():DifficultyLevel {
        var i = config.levelsDifficulty.length;
        while (i-- > 0) {
            var diff = config.levelsDifficulty[i];
            if (level >= diff.levelFrom) {
                return diff;
            }
        }
        return null;
    }
    
    public function update(dt:Float):Void {
        if (currentDelayer != null) {
            currentDelayer.update(dt);
        }
    }
    
    public function getLevel() return level;
    
    public function dispose():Void {
        stop();
    }
}

typedef DifficultyLevel = {
    var wavesTimeout:RangeValue;
    var waveRate:RangeValue;
    var jumpPeriod:RangeValue;
    var levelFrom:Int;
}

typedef RangeValue = {
    min:Float, max:Float
}