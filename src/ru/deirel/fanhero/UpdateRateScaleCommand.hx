package ru.deirel.fanhero;

import mmvc.impl.Command;
import ru.deirel.fanhero.GameModel.WaveRateInfo;

/**
 * ...
 * @author 
 */
class UpdateRateScaleCommand extends Command {
    @inject public var rateScale:RateScaleModel;
    @inject public var rateInfo:WaveRateInfo;
    
    override public function execute():Void {
        rateScale.rate(rateInfo.rate.value);
    }
}