package ru.deirel.fanhero;

import mmvc.impl.Command;
import msignal.Signal.Signal1;

/**
 * ...
 * @author 
 */
class PauseToggleCommand extends Command {
    @inject public var gameModel:GameModel;
    @inject public var on:PauseToggleInfo;
    
    override public function execute():Void {
        gameModel.setPause(on.paused);
    }
}

class PauseToggleSignal extends Signal1<PauseToggleInfo> { public function new() super(PauseToggleInfo); }

class PauseToggleInfo {
    public var paused:Bool;
    public function new(paused:Bool) {
        this.paused = paused;
    }
}