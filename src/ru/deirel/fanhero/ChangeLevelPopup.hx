package ru.deirel.fanhero;

import motion.Actuate;
import motion.easing.Linear;
import motion.easing.Sine;
import starling.display.Sprite;
import starling.text.TextField;
import starling.text.TextFormat;

/**
 * ...
 * @author 
 */
class ChangeLevelPopup extends PopupBase {
    private var up:Bool;
    private var tf1:TextField;
    private var tf2:TextField;
    
    public function new(up:Bool) {
        super();
        this.up = up;
        init();
    } 
    
    override private function create():Void {
        addChild(tf1 = createTextField());
        addChild(tf2 = createTextField());
        tf1.alpha = tf2.alpha = 0.0;
    }
    
    private function createTextField():TextField {
        var tf = new TextFormat("Verdana", 60, up ? 0x00ff00 : 0xff0000);
        tf.bold = true;
        var textField = new TextField(500, 80, up ? "LEVEL UP!" : "LEVEL DOWN!", tf);
        textField.alignPivot();
        return textField;
    }
    
    override private function animate():Void {
        Actuate.tween(tf1, 0.4, { alpha: 1.0 }).ease(Linear.easeNone);
        Actuate.tween(tf1, 0.9, { alpha: 0.0 }, false).onComplete(removeFromParent, [true]).delay(0.4).ease(Linear.easeNone);
        Actuate.apply(tf2, { alpha: 1.0 }).delay(0.4);
        Actuate.tween(tf2, 0.5, { alpha: 0.0, scale: 3.0 }, false).delay(0.4).ease(Sine.easeOut);
    }
    
    override public function dispose():Void {
        Actuate.stop(tf1);
        Actuate.stop(tf2);
        super.dispose();
    }
}