package ru.deirel.fanhero;

import mmvc.impl.Command;
import msignal.Signal.Signal1;
import ru.deirel.fanhero.GameModel;

/**
 * ...
 * @author 
 */
class CreateWaveCommand extends Command {
    @inject public var waveInfo:CreateWaveInfo;
    @inject public var wavesModel:GameModel;
    
    override public function execute():Void {
        wavesModel.createWave(waveInfo.velocity, waveInfo.impulse);
    }
}

class CreateWaveSignal extends Signal1<CreateWaveInfo> { public function new() super(CreateWaveInfo); }

class CreateWaveInfo {
    public var velocity:Float;
    public var impulse:Float;
    public function new(velocity:Float, impulse:Float) {
        this.velocity = velocity;
        this.impulse = impulse;
    }
}