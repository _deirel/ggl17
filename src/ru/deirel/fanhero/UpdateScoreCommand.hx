package ru.deirel.fanhero;

import mmvc.impl.Command;
import ru.deirel.fanhero.GameModel.WaveRateInfo;

/**
 * ...
 * @author 
 */
class UpdateScoreCommand extends Command {
    @inject public var score:GameScoreModel;
    @inject public var rateInfo:WaveRateInfo;
    
    override public function execute():Void {
        score.rate(rateInfo.rate.value);
    }
}