package ru.deirel.fanhero;
import motion.Actuate;
import motion.easing.Linear;
import ru.deirel.fanhero.utils.RGBInterpolator;
import starling.display.Quad;
import starling.display.Sprite;
import starling.text.TextField;
import starling.text.TextFormat;

/**
 * ...
 * @author 
 */
class RateScale extends Sprite {
    private var interpolator:RGBInterpolator;
    private var fg:Quad;
    private var bg:Quad;
    private var label:TextField;
    private var animQuad:Quad;
    private var isShown:Bool = false;
    
    public function new(colors:Array<RGB>) {
        super();
        interpolator = new RGBInterpolator(colors);
        fg = new Quad(400, 40, 0xffffff);
        fg.x = -fg.width / 2;
        fg.width = 0;
        bg = new Quad(400, 40, 0xffffff);
        bg.x = fg.x;
        var tf = new TextFormat("Verdana", 20, 0x000000);
        label = new TextField(400, 40, "", tf);
        label.x = bg.x;
        label.y = bg.y;
        animQuad = new Quad(400, 40, 0xffffff);
        animQuad.alignPivot();
        animQuad.x = bg.x + bg.width / 2;
        animQuad.y = bg.y + bg.height / 2;
        addChild(animQuad);
        addChild(bg);
        addChild(fg);
        addChild(label);
        alpha = 0.0;
        touchable = false;
    }
    
    public function setFill(value:Float, isReset:Bool):Void {
        if (!isShown) {
            isShown = true;
            Actuate.tween(this, 0.4, { alpha: 1.0 }).ease(Linear.easeNone);
        }
        
        if (value < 0) value = 0; else if (value > 1) value = 1;
        
        var currentColor = RGBUtils.getRGB(fg.color);
        var targetColor = interpolator.getColor(value);
        
        Actuate.update(setColor, 0.8, [currentColor.r, currentColor.g, currentColor.b], [targetColor.r, targetColor.g, targetColor.b]);
        Actuate.tween(fg, 0.8, { width: bg.width * value });
        
        if (!isReset) {
            animQuad.color = RGBUtils.getColor(targetColor);
            animQuad.scale = 1.0;
            animQuad.alpha = 0.6;
            Actuate.tween(animQuad, 1.5, { alpha: 0.0, scaleX: 1.5, scaleY: 2.0 });
        }
    }
    
    public function setLabel(value:String):Void {
        label.text = value;
    }
    
    private function setColor(r:Int, g:Int, b:Int):Void {
        fg.color = b | (g << 8) | (r << 16);
        r = Std.int(r * 0.7);
        g = Std.int(g * 0.7);
        b = Std.int(b * 0.7);
        bg.color = b | (g << 8) | (r << 16);
    }
    
    override public function dispose():Void {
        Actuate.stop(setColor);
        Actuate.stop(fg);
        Actuate.stop(animQuad);
        super.dispose();
    }
}

