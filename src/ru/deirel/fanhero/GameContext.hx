package ru.deirel.fanhero;

import mmvc.api.IViewContainer;
import mmvc.impl.Context;
import ru.deirel.fanhero.ChangeLevelCommand.ChangeLevelSignal;
import ru.deirel.fanhero.ChangeLevelCommand.LevelChangedSignal;
import ru.deirel.fanhero.CreateMenCommand.CreateMenSignal;
import ru.deirel.fanhero.CreateWaveCommand.CreateWaveSignal;
import ru.deirel.fanhero.GameModel.WaveRateSignal;
import ru.deirel.fanhero.GameScoreModel.GameScoreChangedSignal;
import ru.deirel.fanhero.PauseToggleCommand.PauseToggleSignal;
import ru.deirel.fanhero.RateScaleModel.AvgRateChangeSignal;

/**
 * ...
 * @author 
 */
class GameContext extends Context {
    var wavesModel:ru.deirel.fanhero.GameModel;
    
    public function new(view:IViewContainer) {
        super(view, false);
    }
    
    override public function startup():Void {
        mediatorMap.mapView(GameView, GameMediator);
        
        wavesModel = new GameModel();
        App.it.updater.add(wavesModel);
        injector.mapValue(GameModel, wavesModel);
        
        injector.mapSingleton(WavesGenerator);        
        injector.mapSingleton(RateScaleModel);
        injector.mapSingleton(GameConfig);
        injector.mapSingleton(AvgRateChangeSignal);
        injector.mapSingleton(LevelChangedSignal);
        injector.mapSingleton(GameScoreChangedSignal);
        injector.mapSingleton(GameScoreModel);
        
        commandMap.mapSignalClass(CreateWaveSignal, CreateWaveCommand);
        commandMap.mapSignalClass(CreateMenSignal, CreateMenCommand);
        commandMap.mapSignalClass(WaveRateSignal, UpdateRateScaleCommand);
        commandMap.mapSignalClass(WaveRateSignal, UpdateScoreCommand);
        commandMap.mapSignalClass(ChangeLevelSignal, ChangeLevelCommand);
        commandMap.mapSignalClass(PauseToggleSignal, PauseToggleCommand);
        
        injector.injectInto(wavesModel);
    }
    
    public function startGame():Void {
        wavesModel.start();        
    }
    
    override public function shutdown():Void {
        App.it.updater.remove(wavesModel);
        wavesModel.dispose();
        wavesModel = null;
    }
}