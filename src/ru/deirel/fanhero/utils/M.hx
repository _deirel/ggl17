package ru.deirel.fanhero.utils;
import haxe.macro.Expr;

/**
 * ...
 * @author 
 */
class M {

    macro static public function dispose(expr:Expr):Expr {
        var id:String = switch (expr.expr) {
            case EConst(CIdent(value)): value;
            default: null;
        };
        return id == null ? macro {} : macro if ($i{id} != null) {
            $i{id}.dispose();
            $i{id} = null;
        };
    }    
}