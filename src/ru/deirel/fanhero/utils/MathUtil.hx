package ru.deirel.fanhero.utils;

/**
 * ...
 * @author 
 */
class MathUtil {

    static public function gte(n1:Float, n2:Float, eps:Float = 0.001):Bool return n1 > n2 || Math.abs(n1 - n2) < eps;
}