package ru.deirel.fanhero.utils;

/**
 * ...
 * @author 
 */
class RGBInterpolator {
    private var keyColors:Array<RGB>;
    private var step:Float;
    
    public function new(keyColors:Array<RGB>) {
        this.keyColors = keyColors;
        this.step = 1 / (keyColors.length - 1);
    }
    
    public function getColor(value:Float):RGB {
        if (value < 0) value = 0; else if (value > 0.99) value = 0.99;
        var id = Math.floor(value / step);
        var p = value - id * step;
        var c1 = keyColors[id];
        var c2 = keyColors[id + 1];
        return RGBUtils.interpolateColor(c1, c2, p);
    }
}

typedef RGB = { r:Int, g:Int, b:Int }

class RGBUtils {
    
    static public function getRGB(c:Int):RGB {
        var r1 = c >>> 16;
        var g1 = (c >>> 8) & 0xff; 
        var b1 = c & 0xff;
        return { r:r1, g:g1, b:b1 };
    }
    
    static public function getColor(c:RGB):UInt {
        return c.b | (c.g << 8) | (c.r << 16);
    }
    
    static public function interpolateColor(c1:RGB, c2:RGB, p:Float):RGB {
        var r = Std.int((1 - p) * c1.r + p * c2.r);
        var g = Std.int((1 - p) * c1.g + p * c2.g);
        var b = Std.int((1 - p) * c1.b + p * c2.b);
        return { r:r, g:g, b:b };
    }
}