package ru.deirel.fanhero.utils;

/**
 * ...
 * @author 
 */
class UniqueId {
    private var _id:Int = 1;
    private var _setFree:Array<Int> = [];
    private var _setFreeCount:Int = 0;
    
    public function new() {
    }
    
    public function get():Int {
        if (_setFreeCount > 0) {
            _setFreeCount--;
            return _setFree.pop();
        }
        return _id++;
    }
    
    public function free(id:Int):Void {
        _setFree.push(id);
    }
}