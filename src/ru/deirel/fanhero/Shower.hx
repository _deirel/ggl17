package ru.deirel.fanhero;
import flash.geom.Point;
import motion.Actuate;
import motion.easing.Elastic;
import motion.easing.Quad;
import starling.display.DisplayObject;

/**
 * ...
 * @author 
 */
class Shower {
    private var objects:Array<DisplayObject>;
    private var positions:Map<DisplayObject, Point> = new Map();
    
    public function new() {
    }

    public function setObjects(arr:Array<DisplayObject>):Void {
        objects = arr;
        objects.reverse();
        for (obj in objects) positions[obj] = new Point(obj.x, obj.y);
    }
    
    public function animate(callback:Void->Void):Void {
        for (obj in objects) {
            obj.x -= App.it.screenWidth;
            obj.y += Math.random() * 180 - 20;
            //obj.rotation = Math.random() * 15 * Math.PI / 180;
            obj.visible = true;
        }
        var i:Int = objects.length;
        var cb = function () {
            if (--i == 0 && callback != null) {
                callback();
            }
        }
        var delay:Float = 0.0;
        for (obj in objects) Actuate.tween(obj, 3, { x: positions[obj].x, y: positions[obj].y, rotation: 0.0 }).delay(delay += 0.1).ease(new ElasticEaseOut(0.1, 0.75)).onComplete(cb);
    }
    
    public function dispose():Void {
        if (objects != null) {
            for (obj in objects) Actuate.stop(obj);
        }
        objects = null;
        positions = null;
    }
}