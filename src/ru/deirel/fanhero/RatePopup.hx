package ru.deirel.fanhero;
import motion.Actuate;
import motion.easing.Linear;
import starling.filters.BlurFilter;
import starling.filters.DropShadowFilter;
import starling.filters.GlowFilter;
import starling.text.TextField;
import starling.text.TextFormat;

/**
 * ...
 * @author 
 */
class RatePopup extends PopupBase {
    private var text:String;
    private var color:UInt;
    
    public function new(text:String, color:UInt) {
        super();
        this.text = text;
        this.color = color;
        init();
    }
    
    override private function create() {
        var tf = new TextFormat("Verdana", 40, color);
        tf.bold = true;
        var textField = new TextField(400, 50, text, tf);
        textField.alignPivot();
        //var filter = new BlurFilter(3, 3);
        //textField.filter = filter;
        addChild(textField);
    }
    
    override private function animate() {
        Actuate.tween(this, 1.2, { y: this.y - 200, alpha: 0.0 }).ease(Linear.easeNone).onComplete(removeFromParent, [true]);
    }
}