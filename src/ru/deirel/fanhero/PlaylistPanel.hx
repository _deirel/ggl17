package ru.deirel.fanhero;
import msignal.Signal.Signal0;
import starling.display.Button;
import starling.display.Sprite;
import starling.events.Event;
import starling.text.TextField;
import starling.text.TextFormat;

/**
 * ...
 * @author 
 */
class PlaylistPanel extends Sprite {
    public var onNext:Signal0 = new Signal0();
    public var onPrev:Signal0 = new Signal0();
    
    private var title:TextField;
    private var btnPrev:Button;
    private var btnNext:Button;
    
    public function new() {
        super();
        
        btnPrev = new Button(App.it.res.buttonLeft);
        addChild(btnPrev);
        
        var tf = new TextFormat("Verdana", 20, 0, "center", "center");
        title = new TextField(300, Std.int(btnPrev.height), ". . .", tf);
        title.x = btnPrev.x + btnPrev.width + 16;
        addChild(title);
        
        btnNext = new Button(App.it.res.buttonRight);
        btnNext.x = title.x + title.width + 16;
        addChild(btnNext);
        
        btnPrev.addEventListener(Event.TRIGGERED, function (_) onPrev.dispatch());
        btnNext.addEventListener(Event.TRIGGERED, function (_) onNext.dispatch());
    }
    
    public function changeTitle(value:String):Void {
        title.text = value;
    }
}