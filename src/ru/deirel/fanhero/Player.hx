package ru.deirel.fanhero;

import flash.media.Sound;
import msignal.Signal.Signal1;
import treefortress.sound.SoundManager;

/**
 * ...
 * @author 
 */
class Player extends SoundManager {
    public var musicChanged:Signal1<Int> = new Signal1();
    
    private var playlists:Map<String, Array<Sound>> = new Map();
    private var currentPlaylist:String = null;
    private var currentPlayingId:Int = 0;
    
    public function new() {
        super();
    }
    
    public function addPlaylist(name:String, sounds:Array<Sound>):Void {
        playlists[name] = sounds;
        for (i in 0...sounds.length) addSound('${name}_$i', sounds[i]);
    }
    
    public function playlist(name:String):Void {
        if (name == currentPlaylist) {
            return;
        }
        var playlist = playlists[name];
        if (playlist != null) {
            currentPlaylist = name;
            playNum(0);
        }
    }
    
    public function playNext():Void {
        playNum(currentPlayingId + 1);
    }
    
    public function playPrev():Void {
        playNum(currentPlayingId - 1);
    }
    
    private function playNum(id:Int):Void {
        if (currentPlaylist == null) {
            return;
        }
        var playlist = playlists[currentPlaylist];
        if (playlist != null) {
            if (id < 0) id = playlist.length - 1; else if (id > playlist.length - 1) id = 0;
            var lastPlaylist = currentPlaylist;
            stopAll();
            currentPlaylist = lastPlaylist;
            play('${currentPlaylist}_$id').soundCompleted.addOnce(function(_) playNext());
            currentPlayingId = id;
            musicChanged.dispatch(currentPlayingId);
        }
    }
    
    override public function stopAll():Void {
        currentPlaylist = null;
        currentPlayingId = 0;
        super.stopAll();
    }
}