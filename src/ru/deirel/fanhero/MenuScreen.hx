package ru.deirel.fanhero;
import flash.ui.Keyboard;
import starling.display.Button;
import starling.events.Event;
import starling.text.TextField;
import starling.text.TextFormat;
import starling.textures.Texture;

/**
 * ...
 * @author 
 */
class MenuScreen extends Screen {
    
    public function new() {
        super();
        init();
    }
    
    private function init():Void {
        var btn = new Button(App.it.res.keyboard);
        btn.alignPivot();
        btn.scale = 0.75;
        btn.x = App.it.screenWidth / 2;
        btn.y = App.it.screenHeight / 2 - 40;
        addChild(btn);
        btn.addEventListener(Event.TRIGGERED, startHandler);
        
        var tft = new TextFormat("Verdana", 28, 0xe88800);
        tft.bold = true;
        var tfd = new TextField(400, 40, "Press [SPACE] to play", tft);
        tfd.alignPivot("center", "top");
        tfd.x = btn.x;
        tfd.y = btn.y + btn.height / 2;
        addChild(tfd);
    }
    
    override public function activate():Void {
        App.it.keys.onKeyPress.add(keyPressHandler);
    }
    
    private function keyPressHandler(keyCode:UInt):Void {
        if (keyCode == Keyboard.SPACE) {
            App.it.keys.onKeyPress.remove(keyPressHandler);
            startHandler();
        }
    }
    
    override public function deactivate():Void {
        App.it.keys.onKeyPress.remove(keyPressHandler);
    }
    
    private function startHandler(?e:Event):Void {
        App.it.screenManager.showScreen(new GameScreen(), true);
    }
}