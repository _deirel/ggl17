package ru.deirel.fanhero;
import flash.Lib;
import flash.errors.Error;
import flash.net.SharedObject;

/**
 * ...
 * @author 
 */
class SharedProperty<T> {
    static private var so(get, never):SharedObject;
    static private var _so:SharedObject = null;
    
    static private function get_so() {
        if (_so == null) {
            try {
                _so = SharedObject.getLocal('fan_hero');
            } catch (err:Error) {}
        }
        return _so;
    }

    private var soLink:SharedObject;
    private var id:String;
    private var value:T;
    
    public function new(id:String, defaultValue:T) {
        this.id = id;
        this.soLink = so;
        if (soLink != null) {
            try {
                value = Reflect.hasField(soLink.data, id) ? cast Reflect.field(soLink.data, id) : defaultValue;
            } catch (err:String) {
                value = defaultValue;
            }
        }
    }
    
    public function write(value:T):Void {
        this.value = value;
        if (soLink != null) {
            soLink.setProperty(id, value);
            soLink.flush();
        }
    }
    
    public function get():T return value;
}