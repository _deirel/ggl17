package ru.deirel.fanhero;
import starling.display.Button;
import starling.events.Event;
import starling.textures.Texture;

/**
 * ...
 * @author 
 */
class ToggleButton extends Button {
    private var handler:Bool->Void;
    private var internalState:Bool = false;    
    private var states:Array<Texture>;
    
    public function new(onState:Texture, offState:Texture) {
		super(offState);
        states = [onState, offState];
        addEventListener(Event.TRIGGERED, _handler);
    }
    
    public function setHandler(foo:Bool->Void):Void {
        handler = foo;
    }
    
    private function _handler(_) {
        set(!internalState);
        if (handler != null) {
            handler(internalState);
        }
    }
    
    public function toggle():Bool {
        set(!internalState);
        return internalState;
    }
    
    private function _set(state:Bool):Bool {
        if (internalState == state) {
            return false;
        }
        internalState = state;
        upState = states[internalState ? 0 : 1];
        return true;
    }
    
    public function set(state:Bool):Void {
        if (_set(state)) {
            onChanged();
        }
    }
    
    private function onChanged():Void {
    }
    
    override public function dispose():Void {
        handler = null;
        states = null;
        super.dispose();
    }
}