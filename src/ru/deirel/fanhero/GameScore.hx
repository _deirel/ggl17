package ru.deirel.fanhero;
import motion.Actuate;
import motion.easing.Linear;
import starling.display.Sprite;
import starling.text.TextField;
import starling.text.TextFormat;

/**
 * ...
 * @author 
 */
class GameScore extends Sprite {
    private var textField:TextField;
    private var lastValue:Float = 0;
    private var isShown:Bool = false;
    
    public function new() {
        super();
        var tf = new TextFormat("Verdana", 28, 0xeebb03, "center", "top");
        tf.bold = true;
        textField = new TextField(400, 50, "Score", tf);
        textField.alignPivot("center", "top");
        addChild(textField);
        alpha = 0.0;
        touchable = false;
    }
    
    public function setScore(value:Float):Void {
        if (!isShown) {
            isShown = true;
            Actuate.tween(this, 0.4, { alpha: 1.0 }).ease(Linear.easeNone);
        }
        Actuate.update(updateScore, 1.5, [lastValue, 0.0], [value, Math.PI]);
    }
    
    private function updateScore(value:Float, t:Float):Void {
        textField.text = "Score " + Math.round(value);
        textField.scale = 1 + 0.05 * Math.sin(t);
        lastValue = value;
    }
    
    override public function dispose():Void {
        Actuate.stop(this);
        Actuate.stop(updateScore);
    }
}