package ru.deirel.fanhero;

import mmvc.impl.Command;
import msignal.Signal.Signal1;
import msignal.Signal.Signal0;

/**
 * ...
 * @author 
 */
class ChangeLevelCommand extends Command {
    @inject public var info:ChangeLevelInfo;
    @inject public var wavesGenerator:WavesGenerator;
    
    override public function execute():Void {
        wavesGenerator.setLevel(info.levelNum);
    }
}

class ChangeLevelInfo {
    public var levelNum:Int;
    public function new(levelNum:Int) {
        this.levelNum = levelNum;
    }
}

class LevelChangedInfo {
    public var levelNum:Int;
    public var up:Bool;
    public function new(levelNum:Int, up:Bool) {
        this.levelNum = levelNum;
        this.up = up;
    }
}

class ChangeLevelSignal extends Signal1<ChangeLevelInfo> { public function new() super(ChangeLevelInfo); }

class LevelChangedSignal extends Signal1<LevelChangedInfo> { public function new() super(LevelChangedInfo); }