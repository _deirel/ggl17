package ru.deirel.fanhero;
import flash.display.BitmapData;
import flash.media.Sound;
import flash.utils.ByteArray;
import flash.xml.XML;
import ru.deirel.fanhero.utils.M;
import starling.textures.Texture;
import starling.textures.TextureAtlas;

/**
 * ...
 * @author 
 */
class Res {
    private var heroAtlas = new TextureAtlas(Texture.fromBitmapData(new BHeroAtlas(0, 0)), new XML(new XHeroAtlas()));
    
    public var buttonBack = Texture.fromColor(32, 32, 0x0000ff);
    public var buttonPlay:Texture;
    public var buttonPause:Texture;
    public var buttonMusicOn:Texture;
    public var buttonMusicOff:Texture;
    public var buttonLeft:Texture;
    public var buttonRight:Texture;
    
    public var body:Texture;
    public var hands:Texture;
    
    public var h0:Texture;
    public var h1:Texture;
    public var h2:Texture;
    public var h3:Texture;
    public var h4:Texture;
    
    public var s0:Texture;
    public var s1:Texture;
    public var s2:Texture;
    public var s3:Texture;
    public var s4:Texture;
    
    public var keyboard = Texture.fromBitmapData(new BKeyboard(0, 0));
    
    public var facesHero:Array<Texture>;
    public var facesSosed:Array<Texture>;
    
    public var sounds:Array<Sound>;

    public function new() {
        buttonPlay = heroAtlas.getTexture("key_play");
        buttonPause = heroAtlas.getTexture("key_pause");
        buttonMusicOn = heroAtlas.getTexture("music_on");
        buttonMusicOff = heroAtlas.getTexture("music_off");
        buttonLeft = heroAtlas.getTexture("left");
        buttonRight = heroAtlas.getTexture("right");
        
        h0 = heroAtlas.getTexture("h0");
        h1 = heroAtlas.getTexture("h1");
        h2 = heroAtlas.getTexture("h2");
        h3 = heroAtlas.getTexture("h3");
        h4 = heroAtlas.getTexture("h4");
        s0 = heroAtlas.getTexture("s0");
        s1 = heroAtlas.getTexture("s1");
        s2 = heroAtlas.getTexture("s2");
        s3 = heroAtlas.getTexture("s3");
        s4 = heroAtlas.getTexture("s4");
        body = heroAtlas.getTexture("hero");
        hands = heroAtlas.getTexture("hands");
        
        facesHero = [h0, h1, h2, h3, h4];
        facesSosed = [s0, s1, s2, s3, s4];
        
        sounds = [
            new STobuInfections(),
            new BensoundActionable(),
            new BensoundHappyrock(),
            new DeafKevInvicible()
        ];
    }
    
    public function dispose():Void {
        M.dispose(buttonBack);
        M.dispose(heroAtlas);
        M.dispose(keyboard);
    }
}

@:bitmap("assets/hero_atlas.png") class BHeroAtlas extends BitmapData {}
@:file("assets/hero_atlas.xml") class XHeroAtlas extends ByteArray {}
@:bitmap("assets/keyboard.png") class BKeyboard extends BitmapData {}

// ---------------- Музыка ----------------
@:sound("assets/music/bensound-actionable.mp3") class BensoundActionable extends Sound {}
@:sound("assets/music/bensound-happyrock.mp3") class BensoundHappyrock extends Sound {}
@:sound("assets/music/DEAF KEV - Invincible.mp3") class DeafKevInvicible extends Sound {}
@:sound("assets/music/Tobu - infections.mp3") class STobuInfections extends Sound {}