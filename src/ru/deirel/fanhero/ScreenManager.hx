package ru.deirel.fanhero;
import starling.display.DisplayObjectContainer;

/**
 * ...
 * @author 
 */
class ScreenManager {
    private var container:DisplayObjectContainer;
    private var stack:Array<Screen> = new Array();
    
    public function new(container:DisplayObjectContainer) {
        this.container = container;
    }
    
    public function showScreen(screen:Screen, replace:Bool = true):Void {
        if (stack.length > 0) {
            var last = stack[stack.length - 1];
            last.deactivate();
            if (replace) {
                stack.pop();
                last.removeFromParent(true);
            } else {
                last.removeFromParent(false);
            }
        }
        
        stack.push(screen);
        container.addChild(screen);
        screen.activate();
    }
    
    public function hideScreen(screen:Screen, dispose:Bool = true):Void {
        var id:Int = stack.indexOf(screen);
        if (id > -1) {
            screen.deactivate();
            screen.removeFromParent(dispose);
            stack.splice(id, 1);
            if (id == stack.length && id > 0) {
                var last = stack[id - 1];
                container.addChild(last);
                last.activate();
            }
        }
    }
    
    public function dispose():Void {
        for (s in stack) s.removeFromParent(true);
        stack = null;
        container = null;
    }
}