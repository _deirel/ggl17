package ru.deirel.fanhero;
import flash.Lib;
import flash.events.FocusEvent;
import flash.events.KeyboardEvent;
import msignal.Signal;

/**
 * ...
 * @author 
 */
class Keys {
    public var onKeyPress:Signal1<UInt> = new Signal1();
    
    private var keyWasDown:Map<UInt, Bool> = new Map();
    
    public function new() {
        Lib.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
        Lib.current.stage.addEventListener(KeyboardEvent.KEY_UP, keyUpHandler);
        Lib.current.stage.addEventListener(FocusEvent.FOCUS_OUT, focusOutHandler);
    }
    
    private function keyDownHandler(e:KeyboardEvent):Void {
        if (keyWasDown[e.keyCode]) {
            return;
        }
        keyWasDown[e.keyCode] = true;
        onKeyPress.dispatch(e.keyCode);
    }
    
    private function keyUpHandler(e:KeyboardEvent):Void {
        keyWasDown[e.keyCode] = false;
    }
    
    private function focusOutHandler(e:FocusEvent):Void {
        Lib.current.stage.focus = null;
    }
    
    public function dispose():Void {
        Lib.current.stage.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
        Lib.current.stage.removeEventListener(KeyboardEvent.KEY_UP, keyUpHandler);
        Lib.current.stage.removeEventListener(FocusEvent.FOCUS_OUT, focusOutHandler);
    }
}