package ru.deirel.fanhero;

import starling.textures.Texture;

/**
 * ...
 * @author 
 */
class MusicButton extends ToggleButton {

    public function new() {
        super(App.it.res.buttonMusicOn, App.it.res.buttonMusicOff);
        _set(App.it.saved.music.get());
        App.it.sound.masterVolume = internalState ? 1.0 : 0.0;
    }
    
    override private function onChanged():Void {
        App.it.sound.masterVolume = internalState ? 1.0 : 0.0;
        App.it.saved.music.write(internalState);
    }
}