package ru.deirel.fanhero;

import mmvc.impl.Mediator;
import ru.deirel.fanhero.ChangeLevelCommand.LevelChangedInfo;
import ru.deirel.fanhero.ChangeLevelCommand.LevelChangedSignal;
import ru.deirel.fanhero.CreateMenCommand.CreateMenSignal;
import ru.deirel.fanhero.CreateWaveCommand.CreateWaveSignal;
import ru.deirel.fanhero.GameModel.WaveHitInfo;
import ru.deirel.fanhero.GameModel.WaveId;
import ru.deirel.fanhero.GameModel.WaveRateInfo;
import ru.deirel.fanhero.GameModel.WaveRateSignal;
import ru.deirel.fanhero.GameScoreModel.GameScoreChangedSignal;
import ru.deirel.fanhero.PauseToggleCommand.PauseToggleInfo;
import ru.deirel.fanhero.PauseToggleCommand.PauseToggleSignal;
import ru.deirel.fanhero.RateScaleModel.AvgRateChangeInfo;
import ru.deirel.fanhero.RateScaleModel.AvgRateChangeSignal;
import ru.deirel.fanhero.utils.RGBInterpolator;
import starling.events.Event;

/**
 * ...
 * @author 
 */
class GameMediator extends Mediator<GameView> {
    @inject public var gameModel:GameModel;
    @inject public var waveSignal:CreateWaveSignal;
    @inject public var createMenSignal:CreateMenSignal;
    @inject public var rateSignal:WaveRateSignal;
    @inject public var rateScale:RateScaleModel;
    @inject public var avgRateCHangeSignal:AvgRateChangeSignal;
    @inject public var config:GameConfig;
    @inject public var levelChangedSignal:LevelChangedSignal;
    @inject public var scoreChangedSignal:GameScoreChangedSignal;
    @inject public var pauseToggleSignal:PauseToggleSignal;
    
    private var rateColors:RGBInterpolator;
    
    private var placeScheme:PlaceScheme = {
        left: 0.0,
        right: 0.0,
        bottom: -20.0,
        indent: 0.0
    };
    
    override public function onRegister():Void {
        view.init(config.rateScaleColors);
        
        rateColors = new RGBInterpolator(config.ratePopupColors);
        
        App.it.updater.add(view);
        App.it.keys.onKeyPress.add(keyPressHandler);
        App.it.sound.musicChanged.add(musicChangedHandler);
        
        gameModel.onIdsAdded.add(idsAddedHandler);
        gameModel.onClear.add(clearHandler);
        gameModel.onHit.add(hitHandler);
        rateSignal.add(rateHandler);
        avgRateCHangeSignal.add(avgRateChangeHandler);
        levelChangedSignal.add(levelChangedSignalHandler);   
        scoreChangedSignal.add(scoreChangedSignalHandler);   
        
        view.addEventListener(GameView.EVENT_PAUSE_CLICKED, pauseClickedHandler);
        view.addEventListener(GameView.EVENT_BACKGROUND_CLICKED, backgroundClickedHandler);
        view.addEventListener(GameView.EVENT_PLAYLIST_NEXT, playlistNextHandler);
        view.addEventListener(GameView.EVENT_PLAYLIST_PREV, playlistPrevHandler);
        
        updateRateScaleLevelLabel();
    }
    
    private function idsAddedHandler(ids:Array<WaveId>):Void {
        var y = App.it.screenHeight - placeScheme.bottom;
        var w = (App.it.screenWidth - placeScheme.left - placeScheme.right - (ids.length - 1) * placeScheme.indent) / ids.length;
        var x = placeScheme.left + w / 2;
        var scale = w / Man.WIDTH;
        for (id in ids) {
            view.addMan(id.id, id.isPlayer, x, y, scale, scale, 0.0, false);
            x += placeScheme.indent + w;
        }
        view.showMen(new Shower(), menShownHandler);
    }
    
    private function menShownHandler():Void {
        gameModel.startGameProcess();
    }
    
    private function clearHandler():Void {
        view.clearMen();
    }
    
    private function hitHandler(info:WaveHitInfo):Void {
        view.jump(info.id.id, config.jumpHeight, info.impulse);
    }
    
    private function rateHandler(rate:WaveRateInfo):Void {
        view.showRatePopup(rate.id.id, config.rateTexts[rate.rate.value], RGBUtils.getColor(rateColors.getColor(rate.rate.value / config.maxRate)));
    }
    
    private function avgRateChangeHandler(info:AvgRateChangeInfo):Void {
        var d = (info.avgRate - config.prevLevelRate) / (config.nextLevelRate - config.prevLevelRate);
        view.setFaces(Math.floor(d * config.numFaces));
        view.setRateScale(d, info.isReset);
    }
    
    private function levelChangedSignalHandler(info:LevelChangedInfo):Void {
        view.showChangeLevelPopup(info.up);
        updateRateScaleLevelLabel();
    }
    
    private function scoreChangedSignalHandler(score:Float):Void {
        view.setScore(score);
    }
    
    private function updateRateScaleLevelLabel():Void {
        view.setRateLabel('Level ${gameModel.getCurrentLevel() + 1}');
    }
    
    private function pauseClickedHandler(e:Event):Void {
        pauseToggleSignal.dispatch(new PauseToggleInfo(e.data.paused));
    }
    
    private function backgroundClickedHandler(e:Event):Void {
        gameModel.hitPlayer();
    }
    
    private function keyPressHandler(keyCode:UInt):Void {
        if (keyCode == config.pauseKeyCode) {
            pauseToggleSignal.dispatch(new PauseToggleInfo(view.togglePauseButton()));
        } else if (keyCode == config.musicKeyCode) {
            view.toggleMusicButton();
        } else if (keyCode == config.playlistNextKeyCode) {
            App.it.sound.playNext();
        } else if (keyCode == config.playlistPrevKeyCode) {
            App.it.sound.playNext();
        }
    }
    
    private function musicChangedHandler(id:Int):Void {
        view.changeMusicTitle(config.musicTitles[id]);
    }
    
    private function playlistNextHandler(_):Void {
        App.it.sound.playNext();
    }
    
    private function playlistPrevHandler(_):Void {
        App.it.sound.playPrev();
    }
    
    override public function onRemove():Void {
        rateColors = null;
        App.it.updater.remove(view);
        App.it.keys.onKeyPress.remove(keyPressHandler);
        App.it.sound.musicChanged.remove(musicChangedHandler);
        gameModel.onIdsAdded.remove(idsAddedHandler);
        gameModel.onClear.remove(clearHandler);
        gameModel.onHit.remove(hitHandler);
        rateSignal.remove(rateHandler);
        avgRateCHangeSignal.remove(avgRateChangeHandler);
        levelChangedSignal.remove(levelChangedSignalHandler);
        scoreChangedSignal.remove(scoreChangedSignalHandler);
        view.removeEventListener(GameView.EVENT_PAUSE_CLICKED, pauseClickedHandler);
        view.removeEventListener(GameView.EVENT_BACKGROUND_CLICKED, backgroundClickedHandler);
        view.removeEventListener(GameView.EVENT_PLAYLIST_NEXT, playlistNextHandler);
        view.removeEventListener(GameView.EVENT_PLAYLIST_PREV, playlistPrevHandler);
    }
}

typedef PlaceScheme = {
    left:Float,
    right:Float,
    bottom:Float,
    indent:Float
}