package ru.deirel.fanhero;
import motion.Actuate;
import starling.display.Sprite;
import starling.events.Event;

/**
 * ...
 * @author 
 */
class PopupBase extends Sprite {
    
    private function create():Void {
    }
    
    private function init():Void {
        create();
        touchable = false;
        addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
    }
    
    private function addedToStageHandler(e:Event):Void {
        removeEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
        animate();
    }
    
    private function animate():Void {
    }
    
    override public function dispose():Void {
        Actuate.stop(this);
        super.dispose();
    }
}