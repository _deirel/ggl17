package ru.deirel.fanhero;
import ru.deirel.fanhero.utils.MathUtil;

/**
 * ...
 * @author 
 */
class Delayer implements IUpdatable {
    private var timeout:Float;
    private var time:Float = 0.0;
    
    public var run:Void->Void;
    
    public function new(timeout:Float) {
        this.timeout = timeout;
    }
    
    public function update(dt:Float):Void {
        if (run != null) {
            time += dt;
            if (MathUtil.gte(time, timeout)) {
                run();
                stop();
            }
        }
    }
    
    public function stop():Void {
        run = null;
        time = 0.0;
    }
}