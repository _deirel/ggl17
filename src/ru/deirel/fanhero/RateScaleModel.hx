package ru.deirel.fanhero;
import mmvc.impl.Actor;
import msignal.Signal.Signal1;
import ru.deirel.fanhero.ChangeLevelCommand.ChangeLevelInfo;
import ru.deirel.fanhero.ChangeLevelCommand.ChangeLevelSignal;
import ru.deirel.fanhero.ChangeLevelCommand.LevelChangedSignal;

/**
 * ...
 * @author 
 */
class RateScaleModel extends Actor {
    @inject public var config:GameConfig;
    @inject public var changeLevelSignal:ChangeLevelSignal;
    @inject public var wavesGenerator:WavesGenerator;
    @inject public var avgRateChangeSignal:AvgRateChangeSignal;
    @inject public var levelChangedSignal:LevelChangedSignal;
    
    private var stats:Array<Float>;
    private var id:Int = 0;
    
    @post public function postConstruct():Void {
        levelChangedSignal.add(levelChangedHandler);
        resetStats();
    }
    
    private function resetStats():Void {
        stats = [for (i in 0...config.rateScaleNumRates) 0.5 * (config.prevLevelRate + config.nextLevelRate)];
    }
    
    public function rate(value:Float):Void {
        if (id < config.rateScaleNumRates) {
            stats[id++] = value;
        } else {
            stats.shift();
            stats.push(value);
        }
        var changed = false;
        var avgRate = calcAverageRate();
        if (avgRate <= config.prevLevelRate) {
            changeLevelSignal.dispatch(new ChangeLevelInfo(wavesGenerator.getLevel() - 1));
            changed = true;
        } else if (avgRate >= config.nextLevelRate) {
            changeLevelSignal.dispatch(new ChangeLevelInfo(wavesGenerator.getLevel() + 1));
            changed = true;
        }
        if (!changed) {
            avgRateChangeSignal.dispatch(new AvgRateChangeInfo(avgRate, false));
        }
    }
    
    private function levelChangedHandler(_):Void {
        resetStats();
        avgRateChangeSignal.dispatch(new AvgRateChangeInfo(calcAverageRate(), true));
    }
    
    private function calcAverageRate():Float {
        var sum = 0.0;
        var wsum = 0.0;
        var l = stats.length;
        for (i in 0...l) {
            var w = (i + 1) * (1 / l);
            wsum += w;
            sum += stats[i] * w;
        }
        return sum / wsum;
    }
    
    public function dispose():Void {
        levelChangedSignal.remove(levelChangedHandler);
    }
}

class AvgRateChangeSignal extends Signal1<AvgRateChangeInfo> { public function new() super(AvgRateChangeInfo); }

class AvgRateChangeInfo {
    public var avgRate:Float;
    public var isReset:Bool;
    public function new(avgRate:Float, isReset:Bool) {
        this.avgRate = avgRate;
        this.isReset = isReset;
    }
}