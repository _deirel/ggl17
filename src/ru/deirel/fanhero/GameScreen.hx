package ru.deirel.fanhero;

/**
 * ...
 * @author 
 */
class GameScreen extends Screen {
    private var context:GameContext;
    private var viewContainer:ru.deirel.fanhero.ViewContainer;
    
    public function new() {
        super();
        viewContainer = new ru.deirel.fanhero.ViewContainer();
        addChild(viewContainer);
		context = new GameContext(viewContainer);
    }
    
    override public function activate():Void {
        context.startup();
        viewContainer.addChild(new GameView());
        context.startGame();
    }
    
    override public function deactivate():Void {
    }
    
    override public function dispose():Void {
        viewContainer.removeFromParent(true);
        viewContainer = null;
        context.shutdown();
        context = null;
        super.dispose();
    }
}