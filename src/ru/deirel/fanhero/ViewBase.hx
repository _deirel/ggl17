package ru.deirel.fanhero;
import starling.display.Sprite;
import starling.events.Event;

/**
 * ...
 * @author 
 */
class ViewBase extends Sprite {
    private var viewContainer:ru.deirel.fanhero.ViewContainer;
    
    public function new() {
        super();
        addEventListener(Event.ADDED_TO_STAGE, _addedToStageHandler);
    }
    
    private function _addedToStageHandler(e:Event):Void {
        try {
            viewContainer = cast(this.parent, ru.deirel.fanhero.ViewContainer);
            viewContainer.add(this);
        } catch (err:String) {
            trace("Ошибка при добавлении вида в контейнер");
            throw err;
        }
    }
    
    private function _removeFromStageHandler(e:Event):Void {
        if (viewContainer != null) {
            viewContainer.remove(this);
            viewContainer = null;
        }
    }
    
    public function isAddedToViewContainer():Bool return viewContainer != null;
}