package ru.deirel.fanhero;
import flash.Lib;

/**
 * ...
 * @author 
 */
class Updater {
    private var items:Array<IUpdatable> = new Array();
    private var itemsToDelete:Array<IUpdatable> = new Array();
    private var inCycle:Bool = false;
    private var inPause:Bool = false;
    
    public function new() {
    }
    
    public function add(item:IUpdatable) {
        var id = items.indexOf(item);
        if (id < 0) {
            items.push(item);
        }
    }
    
    public function remove(item:IUpdatable) {
        var id = items.indexOf(item);
        if (id > -1) {
            if (inCycle) {
                itemsToDelete.push(item);
            } else {
                items.splice(id, 1);
            }
        }
    }
    
    public function update(dt:Float):Void {
        if (inPause) {
            return;
        }
        inCycle = true;
        for (i in items) i.update(dt);
        inCycle = false;
        while (itemsToDelete.length > 0) {
            remove(itemsToDelete.pop());
        }
    }
    
    public function setPause(value:Bool):Void {
        inPause = value;
    }
    
    public function dispose():Void {
        items = null;
    }
}