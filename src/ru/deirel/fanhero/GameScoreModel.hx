package ru.deirel.fanhero;
import mmvc.impl.Actor;
import msignal.Signal.Signal1;
import ru.deirel.fanhero.GameModel.WaveRateInfo;
import ru.deirel.fanhero.GameModel.WaveRateSignal;

/**
 * ...
 * @author 
 */
class GameScoreModel extends Actor {
    @inject public var config:GameConfig;
    @inject public var gameModel:GameModel;
    @inject public var changedSignal:GameScoreChangedSignal;
    
    private var score:Float = 0.0;
    
    public function rate(value:Int):Void {
        if (value == config.maxRate ||
            value == config.maxRate - 1) {
            var mul = value == config.maxRate ? 1.0 : config.scorePenalty;
            //var bonus = config.scoreBase * mul * Math.pow(config.scoreRise, gameModel.getCurrentLevel());
            var bonus = config.scoreBase * mul * (gameModel.getCurrentLevel() + 1);
            score += bonus;
            changedSignal.dispatch(score);
        }
    }
}

class GameScoreChangedSignal extends Signal1<Float> { public function new() super(Float); }
