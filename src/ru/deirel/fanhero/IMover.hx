package ru.deirel.fanhero;
import ru.deirel.fanhero.utils.MathUtil;

/**
 * ...
 * @author 
 */
interface IMover extends IUpdatable {

    function getY():Float;
    function isEnded():Bool;
}

class SineMover implements IMover {
    private var time:Float = 0.0;
    private var y:Float = 0.0;
    private var endTime:Float;
    private var amplitude:Float;
    private var period:Float;
    
    public function new(height:Float, period:Float) {
        this.amplitude = height * 2;
        this.period = period;
        endTime = period;
    }
    
    public function getY() return y;
    
    public function isEnded():Bool return MathUtil.gte(time, endTime);
    
    public function update(dt:Float):Void {
        time += dt;
        y = amplitude * (1 - Math.cos(time * (2 * Math.PI / period)));
    }
}