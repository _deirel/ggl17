package ru.deirel.fanhero;
import ru.deirel.fanhero.utils.RGBInterpolator;
import flash.ui.Keyboard;
import ru.deirel.fanhero.WavesGenerator.DifficultyLevel;
import ru.deirel.fanhero.utils.RGBInterpolator.RGB;
import ru.deirel.fanhero.utils.RGBInterpolator.RGBUtils;

/**
 * ...
 * @author 
 */
class GameConfig {
    public var firstWaveTimeout:Float = 1.0;        // Время до первого запуска волны
    public var rateScaleNumRates:Int = 10;          // Кол-во очков, учитывающихся в статистике для расчета среднего значения
    public var nextLevelRate:Float = 4.2;           // Граница для перехода на след. уровень
    public var prevLevelRate:Float = 1.7;           // Граница для перехода на пред. уровень
    public var maxRate:Int = 5;                     // Макс. оценка
    public var rateSpecificStep:Float = 367;        // Удельный шаг по времени (мс), по которому квантизуется оценка (при скорости волны 1 чел./с)
    public var advancedWorstTime:Float = 150;       // Дополнительное время к наихудшему, прежде чем волна автоматически будет провалена
    public var numFaces:Int = 5;                    // Кол-во лиц персонажей
    public var rateScaleColors:Array<RGB> = [0xff0000, 0xffff00, 0x00ff00].map(RGBUtils.getRGB);    // Переход цвета на шкале очков
    public var ratePopupColors:Array<RGB> = [0xaa0000, 0xaaaa00, 0x00aa00].map(RGBUtils.getRGB);    // Переход текста на попапе о получении очков
    public var rateTexts:Array<String> = [          // Расшифровка оценок
        "Missed!",
        "Terrible!",
        "Bad!",
        "Good!",
        "Fine!",
        "Fantastic!"
    ];
    public var jumpHeight:Float = 30;               // Высота прыжка (в пикселях)
    public var jumpDefaultPeriod:Float = 0.4;       // Период (время) прыжка по умолчанию, если игрок прыгает не в счет волны
    public var scoreBase:Float = 10.0;              // Базовое кол-во очков за "Великолепно"
    public var scorePenalty:Float = 0.5;            // Штраф к оценке "Хорошо"
    public var scoreRise:Float = 1.1;               // Степенной показатель роста
    public var pauseKeyCode:UInt = Keyboard.P;      // Кнопка паузы
    public var musicKeyCode:UInt = Keyboard.M;      // Кнопка музыки
    public var playlistNextKeyCode:UInt = Keyboard.X;      // Кнопка след. композиции
    public var playlistPrevKeyCode:UInt = Keyboard.Z;      // Кнопка пред. композиции
    
    public var musicTitles:Array<String> = [
        "Tobu - infections",
        "Bensound - actionable",
        "Bensound - happyrock",
        "Deaf kev - Invicible"
    ];
    
    public var levelsDifficulty:Array<DifficultyLevel> = [      // Сложность уровней
        {
            wavesTimeout: { min: 0.45, max: 1.5 },              // Промежуток времени между двумя волнами 
            waveRate: { min: 0.15, max: 0.15 },                 // Время между двумя чуваками (обратно пропорционально скорости волны)
            jumpPeriod: { min: 0.5, max: 0.7 },                 // Длительность прыжка игрока - от взлета до приземления
            levelFrom: 0
        },
        {
            wavesTimeout: { min: 0.4, max: 1 },
            waveRate: { min: 0.13, max: 0.15 },
            jumpPeriod: { min: 0.45, max: 0.65 },
            levelFrom: 1
        },
        {
            wavesTimeout: { min: 0.35, max: 0.8 },
            waveRate: { min: 0.11, max: 0.15 },
            jumpPeriod: { min: 0.4, max: 0.6 },
            levelFrom: 2
        },
        {
            wavesTimeout: { min: 0.32, max: 0.65 },
            waveRate: { min: 0.1, max: 0.15 },
            jumpPeriod: { min: 0.4, max: 0.6 },
            levelFrom: 3
        },
        {
            wavesTimeout: { min: 0.3, max: 0.5 },
            waveRate: { min: 0.1, max: 0.15 },
            jumpPeriod: { min: 0.4, max: 0.6 },
            levelFrom: 4
        },
        {
            wavesTimeout: { min: 0.25, max: 0.45 },
            waveRate: { min: 0.1, max: 0.13 },
            jumpPeriod: { min: 0.4, max: 0.6 },
            levelFrom: 5
        },
        {
            wavesTimeout: { min: 0.20, max: 0.40 },
            waveRate: { min: 0.1, max: 0.12 },
            jumpPeriod: { min: 0.4, max: 0.6 },
            levelFrom: 7
        },
        {
            wavesTimeout: { min: 0.15, max: 0.30 },
            waveRate: { min: 0.095, max: 0.10 },
            jumpPeriod: { min: 0.3, max: 0.5 },
            levelFrom: 11
        },
        {
            wavesTimeout: { min: 0.12, max: 0.24 },
            waveRate: { min: 0.09, max: 0.1 },
            jumpPeriod: { min: 0.3, max: 0.5 },
            levelFrom: 15
        }
    ];
    
    public function new() {        
    }
}