package ru.deirel.fanhero;

/**
 * ...
 * @author 
 */
class Rater {
    private var specificStep:Float;
    private var maxRate:Int;
    private var advancedWorstTime:Float;
    
    public function new(specificStep:Float, maxRate:Int, advancedWorstTime:Float) {
        this.specificStep = specificStep;
        this.maxRate = maxRate;
        this.advancedWorstTime = advancedWorstTime;
    }
    
    public function getRate(dtime:Float, waveVelocity:Float):Rate {
        var step = specificStep / waveVelocity;
        var ms = dtime * 1000;
        var rate = maxRate - Math.floor(ms / step);
        if (rate < 0) {
            rate = 0;
        }
        return { value: rate };
    }
    
    public function getWorstDistance(velocity:Float):Float {
        return (maxRate * specificStep / velocity + advancedWorstTime) * velocity / 1000;
    }
    
    public function getWorstRate():Rate {
        return { value: 0 };
    }
    
    public function getTooBadToRemember():Rate {
        return { value: 1 };
    }
}

typedef Rate = {
    var value:Int;
}