package ru.deirel.fanhero;

import starling.display.Button;
import starling.events.Event;
import starling.textures.Texture;

/**
 * ...
 * @author 
 */
class PauseButton extends ToggleButton {

    public function new() {
		super(App.it.res.buttonPlay, App.it.res.buttonPause);
    }
}