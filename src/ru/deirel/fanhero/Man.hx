package ru.deirel.fanhero;
import ru.deirel.fanhero.IMover.SineMover;
import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;
import starling.display.Image;
import starling.display.Quad;
import starling.display.Sprite;
import starling.textures.Texture;

/**
 * ...
 * @author 
 */
class Man {
    static public var WIDTH = 214;
    
    public var graphics:DisplayObjectContainer;
    
    private var manGraphics:ManGraphics;
    
    private var jumpImpulse:Float = 0.0;
    
    private var time:Float = 0.0;
    private var wasSquatFlag:Bool = true;
    
    private var movers:Array<IMover> = new Array();

    public function new(isPlayer:Bool) {
        graphics = new Sprite();
        manGraphics = new ManGraphics(isPlayer);
        graphics.addChild(manGraphics);
        graphics.touchable = false;
    }
    
    public function jump(height:Float, impulse:Float):Void {
        movers.push(new SineMover(height, impulse));
    }
    
    public function update(dt:Float):Void {
        var y = 0.0;
        var i = movers.length;
        while (i-- > 0) {
            var m = movers[i];
            m.update(dt);
            if (m.isEnded()) {
                movers.splice(i, 1);
            } else {
                y += m.getY();
            }
        }
        manGraphics.y = -y;
    }
    
    public function setFace(id:Int) manGraphics.setFace(id);
    
    public function dispose():Void {
        graphics.removeFromParent(true);
        graphics = null;
    }
}

class ManGraphics extends Sprite {
    static private var facesOffsetsY = [
        [3, 9, 17, 3, 32],
        [22, 13, 10, 3, 32]
    ];
    static private var facesOffsetsX = [
        [0, 0, 0, 0, 22],
        [0, 0, 0, 0, -20]
    ];
    
    public var torso:Image;
    public var face:Image;
    public var hands:Image;
    
    private var isPlayer:Bool;
    private var faceId:Int;
    
    public function new(isPlayer:Bool) {
        super();
        this.isPlayer = isPlayer;
        
        torso = new Image(App.it.res.body);
        torso.x = -torso.width / 2;
        torso.y = -torso.height;
        
        hands = new Image(App.it.res.hands);
        hands.x = -hands.width / 2;
        hands.y = torso.y;
        
        if (this.isPlayer) {
            torso.color = 0xaaffaa;
        }
        
        addChild(torso);
        setFace(2);
        addChild(hands);
    }
    
    public function setFace(id:Int):Void {
        var faces = isPlayer ? App.it.res.facesHero : App.it.res.facesSosed;
        if (id < 0) id = 0; else if (id > faces.length - 1) id = faces.length - 1;
        if (face == null) {
            addChild(face = new Image(faces[id]));
        } else {
            face.texture = faces[id];
        }
        faceId = id;
        alignFace();
    }
    
    private function alignFace():Void {
        face.alignPivot();
        face.x = torso.x + torso.width / 2 + 3 + facesOffsetsX[isPlayer ? 1 : 0][faceId];
        face.y = torso.y + 24 + facesOffsetsY[isPlayer ? 1 : 0][faceId];
        face.scale = hands.width / (face.width / face.scale) * 0.8;
    }
}