package;

import flash.Lib;
import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.system.Security;
import starling.core.Starling;

/**
 * ...
 * @author 
 */
class Main extends Sprite {
	static public var starling:Starling;
    
	static function main() {
        Lib.current.addChild(new Main());
	}
	
    public function new() {
        super();
        
        Security.allowDomain("*");
        
		var stage = Lib.current.stage;
		stage.scaleMode = StageScaleMode.NO_SCALE;
		stage.align = StageAlign.TOP_LEFT;
        
        starling = new Starling(App, Lib.current.stage);
        //starling.showStatsAt("right", "top");
        starling.start();
    }
}