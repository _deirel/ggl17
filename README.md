# README #

This game was developed for GlobalGameJam 2017 contest (http://globalgamejam.org)

### Goal of the game ###

Your hero is a concert fan. Jump synchronously with other fans, earn scores and go as far as you can!

### Team ###

* Development: Petr K. (Deirel)
* Game design: Filipp K. (Cancel)